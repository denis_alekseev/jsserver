var express = require('express');
var jsonServer = require('json-server');
const fileUpload = require('express-fileupload');
var morgan = require('morgan')
var multer  =   require('multer');
var server = express();

server.use('/api', jsonServer.router('db.json'));
server.use(fileUpload());
server.use(morgan('combined'))
server.set('views', './views')
server.set('view engine', 'pug')
server.use(express.static('assets'))

server.get('/', function(req, res){
    res.render('index', { title: 'Hey', message: 'Hello there!'});
})

server.post("/upload", function(req,res) {
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './upload');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now());
  }
});
var upload = multer({ storage : storage}).any();
  if (Object.keys(req.files).length == 0) {
    return res.status(400).send('No files were uploaded.');
  }

  let myFile = req.files.myfile;
  let name = "static/" + myFile.name
  myFile.mv(name)
  var num = jsonServer.router('db.json').db.get('posts').size().value();
  let obj = {'id': num, 'name': req.body.names, 'img': name}
  console.log(jsonServer.router('db.json').db);
  jsonServer.router('db.json').db.get('posts').push(obj).write();
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({ name: myFile.name }));
  res.end();

})

server.listen(3000);